import ReactDOM from "react-dom"

import Router from "./Router"

import "./index.scss"

if (module.hot) module.hot.accept()

ReactDOM.render(
  Router(),
  document.getElementById("root")
)
