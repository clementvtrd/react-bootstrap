import React from "react"
import Count from "./Count"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"

export default
function Root() {
  return (
    <Router>
      <nav>
        <ul>
          <li>
            <Link to={"/"}>Accueil</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route exact path="/">
          <Count/>
        </Route>
      </Switch>
    </Router>
  )
}
