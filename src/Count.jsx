import React, { Component, Fragment } from "react"

class Count extends Component {
  constructor() {
    super()
    this.state = {
      count: 0
    }
  }
  render() {
    const count = this.state.count
    return <Fragment>
      <p>
				Button pressed {count} time{count > 1 ? "s" : null}
      </p>
      <button
        onClick={
          () => this.setState({ count: count + 1 })
        }
      >
				+
      </button>
    </Fragment>
  }
}

export default Count
